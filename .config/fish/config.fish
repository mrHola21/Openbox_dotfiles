set fish_greeting

alias ls='exa -a --color=always --group-directories-first --icons'
alias lsa='exa -al --color=always --group-directories-first --icons'
alias tree='exa -aT --color=always --group-directories-first --icons'

alias cat='bat'

alias please='sudo'

alias install='sudo pacman -S'
alias update='sudo pacman -Syy'
alias upgrade='sudo pacman -Syyuu'

if status is-interactive
	python3 ~/Desktop/Openbox_Dotfiles/Helpers/Helpers/Greeter/greeter.py | lolcat
    # Commands to run in interactive sessions can go here
end
