# Terminal emulators

Xterm (pacman)

Alacritty (pacman)

# Shell

Fish (pacman)

# Text editors

Vi (pacman)

Vim (pacman)

Kate (pacman)

Nano (pacman)

# Multimedia

VLC media player (pacman)

# Webbrowsers

firefox (pacman)

# Filemanagers 

Thunar (pacman)

# System

obconf (pacman)

plank (pacman)

ob-autostart (yay)

obkey (yay)

polybar (yay)

lxhotkeys (pacman)

picom (pacman)

lxappearence (pacman)

lxdm (pacman)

lxinput (pacman)

tumbler (pacman)

mpd (pacman)

mpc (pacman)

ncmpcpp (pacman)

glava (pacman)

# Terminal Utils

lolcat (pacman)

neofetch (pacman)

unzip (pacman)


