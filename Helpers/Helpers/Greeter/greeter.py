import datetime
import getpass
import calendar
import argparse


def print_msg_box(msg, indent=1, width=None, title=None):
    lines = msg.split('\n')
    space = " " * indent
    if not width:
        width = max(map(len, lines))
    box = f'╔{"═" * (width + indent * 2)}╗\n'
    if title:
        box += f'║{space}{title:<{width}}{space}║\n'
        box += f'║{space}{"-" * len(title):<{width}}{space}║\n'
    box += ''.join([f'║{space}{line:<{width}}{space}║\n' for line in lines])
    box += f'╚{"═" * (width + indent * 2)}╝'
    print(box)


today = datetime.datetime.now()
hour = int(datetime.datetime.now().hour)

my_parser = argparse.ArgumentParser()

my_parser.add_argument('--bday', '-b', action='store', type=int, nargs=3)
args = my_parser.parse_args()
if args.bday:
    if args.bday[0] == today.day & args.bday[1] == today.month:
        print_msg_box(
            f'Today is {args.bday[0]} of {calendar.month_abbr[args.bday[1]]}\nHappy Birthday {getpass.getuser().capitalize()}!\nYou are {today.year - args.bday[-1]} years old\n')


if 0 <= hour < 12:
    greet = "Good Morning"
elif 12 <= hour < 17:
    greet = "Good Afternoon"
else:
    greet = "Good Evening"

print(
    f'{greet} {getpass.getuser().capitalize()}\nDate : {today.day} - {today.strftime("%B")}\nTime : {today.hour}:{today.minute} ')
