import argparse
import sys
import fileinput


class Notes:
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.arguments()
        self.args = self.parser.parse_args(['remove', 'make bootloader', 'learn git'])

        self.valid = ['add', 'remove', 'list']

    def arguments(self):
        self.parser.add_argument('command')
        self.parser.add_argument('object', nargs='*')

    def route(self):
        if self.args.command in self.valid:
            if self.args.command == self.valid[0]:
                with open('notes.txt', 'a') as f:
                    f.write('\n'.join([val for val in self.args.object]))
            elif self.args.command == self.valid[1]:
                print('k')
                with open('notes.txt', 'r') as f:
                    print(self.args.object)
                    x = f.readlines()
                    print(x)
                    for y in range(len(self.args.object)):
                        print(self.args.object[y])
                        x.pop(0)
        else:
            sys.exit(self.parser.print_help())


Notes().route()
