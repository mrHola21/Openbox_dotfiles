import pyautogui 
import shutil
import os
import getpass 

pyautogui.screenshot().save('screenshot.jpg')
try:
    try:
        shutil.move('screenshot.jpg',f'/home/{getpass.getuser()}/Pictures/ScreenShot')
    except FileNotFoundError : # If no folder 
        os.mkdir(f'/home/{getpass.getuser()}/Pictures/ScreenShot')
        shutil.move('screenshot.jpg',f'/home/{getpass.getuser()}/Pictures/ScreenShot')

except shutil.Error:   
    os.chdir(f'/home/{getpass.getuser()}/Pictures/ScreenShot/')
    files=os.listdir()
    for num in range(len(files)):
        name , ext =files[0].split('.')
        os.rename(files[0],name+str(num)+'.'+ext)
        files.remove(files[0])
    os.chdir(f'/home/{getpass.getuser()}/')
    shutil.move('screenshot.jpg',f'/home/{getpass.getuser()}/Pictures/ScreenShot')
