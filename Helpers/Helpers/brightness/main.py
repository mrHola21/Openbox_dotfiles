import subprocess 
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--increase','-i',type=int)
parser.add_argument('--decrease','-d',type=int)
current_brightness=subprocess.run('xbacklight',shell=True,capture_output=True).stdout.decode()
output = parser.parse_args()
if output.increase:
    value = output.increase
    subprocess.run(f'xbacklight -inc {value}',shell=True)
else:
    value = output.decrease
    if value >= 100:
        value -=1
    print(value)
    subprocess.run(f'xbacklight -dec {value}',shell=True)

